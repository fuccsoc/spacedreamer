import {
  Page,
  Text,
  Image,
  Badge,
  Spacer,
} from "@geist-ui/core";

import Head from "next/head";

import Projects from "./components/projects";
import Contacts from "./components/contacts";

Array.prototype.random = function () {
  return this[Math.floor((Math.random()*this.length))];
}

export default function Index() {
  return (
    <Page dotBackdrop={true} style={{ width: "100%" }}>
      <Head>
        <title>fuccsoc | leetheartz</title>
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/>
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
        <meta name="theme-color" content="#00008B"/>
      </Head>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Image
          src="/ppic_cropped.png"
          style={{
            borderRadius: "100%",
            width: "150px",
            height: "150px",
            objectFit: "cover",
            border: "none",
          }}
          draggable = {false}
        />
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text color="black" style={{ textAlign: "center" }} h2>
          fuccsoc{" "}<Badge type="error">1337heartz team</Badge>{" "}
        </Text>
      </div>
      <Spacer h={1}/>
      <Projects />
      <Spacer h={1}/>
      <Contacts />
    </Page>
  );
}
