import { Card, Grid, Text, Link } from "@geist-ui/core";

export default function () {
  return (
    <>
      <Text h3 style={{ textAlign: "center" }}>
        Contacts
      </Text>
      <Grid.Container justify="center" gap={2}>
        <Grid xs={24} md={5}>
          <Link href="//t.me/fuccsoc2" width="100%">
            <Card hoverable style={{ backgroundColor: "#229ED9" }} width="100%">
              <Text style={{ color: "#FFF", textAlign: "center", margin: 0 }} h5>
                Telegram<br/>
                @fuccsoc2
              </Text>
            </Card>
          </Link>
          </Grid>
          <Grid xs={24} md={5}>
          <Link href="//vk.com/fuccsoc" width="100%">
          <Card hoverable style={{backgroundColor: "#0077FF", textAlign:"center"}} width="100%">
              <Text style={{ color: "#FFF", textAlign: "center", margin: 0 }} h5>
              VK<br/>
              @fuccsoc
              </Text>
          </Card>
          </Link>
        </Grid>
        <Grid xs={24} md={5}>
          <Link href="//discord.com/users/816409266608930867" width="100%">
          <Card hoverable  style={{backgroundColor: "#5865F2", textAlign:"center"}} width="100%">
              <Text style={{ color: "#FFF", textAlign: "center", margin: 0 }} h5>
              Discord<br/>
              fuccsoc#8295
              </Text>
          </Card>
          </Link>
        </Grid>
        <Grid xs={24} md={5}>
          <Link href="mailto:fuccsoc@yandex.ru" width="100%">
          <Card hoverable type="cyan" style={{textAlign:"center"}} width="100%">
              <Text style={{ color: "#FFF", textAlign: "center", margin: 0 }} h5>
              E-mail<br/>
              fuccsoc@yandex.ru
              </Text>
          </Card>
          </Link>
        </Grid>
      </Grid.Container>
    </>
  );
}
