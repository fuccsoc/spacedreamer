import { Text, Image, Card, Grid, Divider, Link } from "@geist-ui/core";

export default function () {
  return (
    <>
      <Text style={{ textAlign: "center" }} h3>
        Skills and projects
      </Text>

      <Grid.Container justify="center" gap={1}>
        <Grid xs={24} md={8}>
          <Card type={"dark"} shadow width="100%">
            <Card.Content>
              <Text h3>Python</Text>
            </Card.Content>
            <Divider />
            <Card.Content>
              <Text p>
                First language that I've learned <br />
                My first steps done using it
              </Text>
              <Text h6>
                Private projects <br />
                <Text type="secondary" span>
                  I decided to hide the source of this to ensure privacy. If you
                  have objective reasons (for example, you're from{" "}
                  <a href="https://ispring.institute">iSpring institute</a>),
                  you can contact me and I'll provide archives with source code.
                </Text>
              </Text>
              <ul>
                <li>
                  <Link href="#" style={{ color: "#FFF" }}>
                    SpringSuite tests results handler
                  </Link>
                </li>
                <li>
                  <Link href="//api.fuccsoc.com/lastfm/user/me/now" icon color>
                    API
                  </Link>
                </li>
                <li>
                  <Link href="//gitlab.com/fuccsoc/bells" icon color>
                    School bells management system
                  </Link>
                </li>
                <li>
                  <Link href="//gitlab.com/fuccsoc/whoisvkbot/" icon color>
                    WHOIS for VK Telegram bot
                  </Link>
                </li>
              </ul>
              <Text h6>Public projects</Text>
              <ul>
                <li>
                  <Link href="//github.com/Dragon-Userbot" icon color>
                    Dragon-Userbot
                  </Link>
                </li>
                <ul>
                  <li>
                    <Link
                      href="//github.com/Dragon-Userbot/Dragon-Userbot"
                      icon
                      color
                    >
                      Core contributor & organisation co-owner
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="//github.com/Dragon-Userbot/custom_modules"
                      icon
                      color
                    >
                      Custom modules contributor
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="//github.com/Dragon-Userbot/custom_modules/blob/main/spotify.py"
                      icon
                      color
                    >
                      Spotify module
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="//github.com/Dragon-Userbot/custom_modules/blob/main/lastfm.py"
                      icon
                      color
                    >
                      Last.FM module
                    </Link>
                  </li>
                </ul>
                <li>
                  <Link href="//gitlab.com/leetheartz/url_bot" icon color>
                    URL shortener TG bot
                  </Link>
                </li>
                <li>
                  <Link href="//gitlab.com/leetheartz/MinakoPython" icon color>
                    Rewrite MinakoAyno bot to python3
                  </Link>
                </li>
                <li>
                  <Link href="//github.com/fuccsoc/NeedHelpBot" icon color>
                    Simple bot, that mention helpers if someone need help
                  </Link>
                </li>
              </ul>
            </Card.Content>
          </Card>
        </Grid>
        <Grid xs={24} md={8}>
          <Card shadow type="cyan" width="100%">
            <Card.Content>
              <Text h3>Kotlin</Text>
            </Card.Content>
            <Divider />
            <Card.Content>
              <Text p>
                Language, that I learned by myself - without any guides: just
                using official documentations and by working with other
                developers's code
              </Text>
              <Text h6>
                Projects <br />
                <Text type="secondary" span>
                  I decided to hide the source of this to ensure privacy. If you
                  have objective reasons (for example, you're from{" "}
                  <a href="https://ispring.institute">iSpring institute</a>),
                  you can contact me and I'll provide archives with source code.
                </Text>
              </Text>
              <ul>
                <li>
                  <Link href="//gitlab.com/fucchub/minakobot" icon color>
                    The first version of MinakoAyno bot, writted with my help by
                    MinakoNyaa
                  </Link>
                </li>
                <li>
                  <Link
                    href="https://gitlab.com/leetheartz/MinakoAinoBot-KT"
                    icon
                    color
                  >
                    My rewrite of MinakoAyno bot on Kotlin
                  </Link>
                </li>
              </ul>
            </Card.Content>
          </Card>
        </Grid>
        <Grid xs={24} md={8}>
          <Card type={"lite"} shadow width="100%">
            <Card.Content>
              <Text h3>WEB stack (React/Next.js/HTML/CSS)</Text>
            </Card.Content>
            <Divider />
            <Card.Content>
              <Text p>
                I've learned this just to make all my projects presentable. It
                wasn't complicated for me - anything here well documented and
                intuitive
              </Text>
              <Text h5>Projects</Text>
              <ul>
                <li>
                  <Link href="//sh.fuccsoc.com" color icon>
                    Simple disclaimer
                  </Link>
                </li>
                <li>
                  <Link href="//fuccsoc.com" color icon>
                    Simple site for agregate my contacts
                  </Link>
                </li>
                <li>
                  <Link href="#" style={{ color: "#000" }}>
                    My personal site (hidden due to confidencial reasons)
                  </Link>
                </li>
              </ul>
            </Card.Content>
          </Card>
        </Grid>
      </Grid.Container>
    </>
  );
}
